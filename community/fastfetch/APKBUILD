# Contributor: Carter Li <zhangsongcui@live.cn>
# Maintainer: Carter Li <zhangsongcui@live.cn>
pkgname=fastfetch
pkgver=2.15.0
pkgrel=0
pkgdesc="Like neofetch, but much faster because written mostly in C."
url="https://github.com/fastfetch-cli/fastfetch"
arch="all"
license="MIT"
depends="
	hwdata-pci
	"
makedepends="
	cmake samurai
	yyjson-dev
	yyjson-static
	vulkan-loader-dev
	libxcb-dev
	wayland-dev
	libdrm-dev
	dconf-dev
	imagemagick-dev
	chafa-dev
	zlib-dev
	dbus-dev
	mesa-dev
	opencl-dev
	xfconf-dev
	sqlite-dev
	networkmanager-dev
	pulseaudio-dev
	ddcutil-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/fastfetch-cli/fastfetch/archive/refs/tags/$pkgver.tar.gz"


prepare() {
	default_prepare

	rm -rf src/3rdparty/yyjson
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		local crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=Release \
		-DENABLE_SYSTEM_YYJSON=ON \
		-DENABLE_DIRECTX_HEADERS=OFF \
		$crossopts
	cmake --build build --target fastfetch --target flashfetch
}

check() {
	build/fastfetch --list-features
	build/fastfetch -c presets/ci.jsonc
	build/fastfetch -c presets/ci.jsonc --format json
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c2934432d23610e34d10cb66b13b0652619bdcb203dbc439fcd5e59a6a7814045ea90bf4d5dae12247f19e222a350cafee1184300845817d03ce6b0bcd947d2d  fastfetch-2.15.0.tar.gz
"

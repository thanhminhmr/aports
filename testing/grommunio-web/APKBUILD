# Maintainer: Noel Kuntze <noel.kuntze@contauro.com>
pkgname=grommunio-web
pkgver=3.8
pkgrel=1
pkgdesc="Web user interface for grommunio"
# s390x: blocked by grommunio-gromox
arch="noarch !riscv64 !s390x"
url="https://grommunio.com/"
license="AGPL-3.0-only"
options="!check" # No test suite
_php=php83
install="$pkgname.pre-install $pkgname.pre-upgrade"
depends="grommunio-gromox
	grommunio-mapi-header-php
	$_php
	$_php-gettext
	$_php-bcmath
	$_php-ctype
	$_php-curl
	$_php-dom
	$_php-gd
	$_php-gettext
	$_php-iconv
	$_php-mbstring
	$_php-openssl
	$_php-sodium
	$_php-sqlite3
	$_php-sysvshm
	$_php-zip
	"

makedepends="libxml2-utils $_php-gettext npm gettext"

pkgusers="grommunio groweb"
pkggroups="grommunio groweb"

source="https://github.com/grommunio/grommunio-web/archive/refs/tags/grommunio-web-$pkgver.tar.gz
	0001-makefile.patch
	"

builddir="$srcdir/grommunio-web-grommunio-web-$pkgver"

build() {
	make -j1
}

package() {
	# webapp
	install -dm0755 "$pkgdir"/usr/share/webapps/
	cp -rp deploy "$pkgdir"/usr/share/webapps/$pkgname

	# license
	install -Dm644 LICENSE.txt "$pkgdir"/usr/share/licenses/$pkgname/LICENSE

	# nginx conf
	install -Dpm644 build/grommunio-web.conf "$pkgdir"/usr/share/grommunio-common/nginx/locations.d/grommunio-web.conf
	install -Dpm644 build/grommunio-web-upstream.conf "$pkgdir"/usr/share/grommunio-common/nginx/upstreams.d/grommunio-web.conf

	# php-fpm
	sed -i "s@/php-fpm/@/php-fpm${_php##php}/@" build/pool-grommunio-web.conf
	install -Dpm644 build/pool-grommunio-web.conf "$pkgdir"/etc/$_php/php-fpm.d/pool-grommunio-web.conf

	# web config
	install -Dm644 config.php.dist "$pkgdir"/etc/grommunio-web/config.php
	ln -sf "/etc/grommunio-web/config.php" "$pkgdir"/usr/share/webapps/grommunio-web/config.php
	rm "$pkgdir"/usr/share/webapps/grommunio-web/config.php.dist
	rm "$pkgdir"/usr/share/webapps/grommunio-web/debug.php.dist

	# plugin config
	for dir in "$pkgdir"/usr/share/webapps/grommunio-web/plugins/*; do
		plugindir=$(basename "$dir")
		if [ -f "$pkgdir"/usr/share/webapps/grommunio-web/plugins/"$plugindir"/config.php ]; then
			mv "$pkgdir"/usr/share/webapps/grommunio-web/plugins/"$plugindir"/config.php "$pkgdir"/etc/grommunio-web/config-"$plugindir".php
			ln -s /etc/grommunio-web/config-"$plugindir".php "$pkgdir"/usr/share/webapps/grommunio-web/plugins/"$plugindir"/config.php
		fi
	done

	# create index and temp directories
	for i in /var/lib/grommunio-web \
		/var/lib/grommunio-web/session \
		/var/lib/grommunio-web/sqlite-index \
		/var/lib/grommunio-web/tmp; do
		install -dm 0770 -g groweb -o groweb "$pkgdir"/$i
	done
}

sha512sums="
f7223aac98e77cbfcd504a611507590b7a5389f00b5f89db743b3f1101ff3cd8504116ffa30889821dd7cb0fe329722ab5ba13c9155e2bfce7792868967b86c2  grommunio-web-3.8.tar.gz
fcfb070ab0bb8cca6f24221fe66eeac7e0c33981c16fff947db7509cc73d39b086710e5fd75bce34a230fa18bc10828d87410d98d147d31cc6ae1c16c41b48ae  0001-makefile.patch
"

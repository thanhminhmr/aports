# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=flawz
pkgver=0.2.1
pkgrel=0
pkgdesc="A Terminal UI for browsing CVEs"
url="https://github.com/orhun/flawz"
arch="all"
license="MIT OR Apache-2.0"
depends="openssl sqlite"
makedepends="cargo cargo-auditable openssl-dev sqlite-dev"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/orhun/flawz/archive/v$pkgver.tar.gz"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
	mkdir -p man
	OUT_DIR=man/ target/release/flawz-mangen
	mkdir -p completions
	OUT_DIR=completions/ target/release/flawz-completions
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 "man/$pkgname.1" -t "$pkgdir/usr/share/man/man1"
	install -Dm 644 "completions/$pkgname.bash" "$pkgdir/usr/share/bash-completion/completions/$pkgname"
	install -Dm 644 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d"
	install -Dm 644 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions"
}

sha512sums="
4bfdde3494de5e9954cf8ac85589794dc298dd41890fee4984bc388a75c3f861b7baa66eeb61a018ab172de97fc25e3db45ee2bf7de3a8b90eca1f6302b9e300  flawz-0.2.1.tar.gz
"
